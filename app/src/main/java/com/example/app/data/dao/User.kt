package com.example.app.data.dao

import androidx.room.*

@Entity(tableName = "user")
data class User(
    @PrimaryKey(autoGenerate = true) val id: Long?,
    @ColumnInfo(name = "name")
    val name: String,
    @ColumnInfo(name = "score")
    val score: Int
)

@Dao
interface UserDao {
    @Query("SELECT * FROM user WHERE id=:id ")
    suspend fun findById(id: Long): User

    @Query("SELECT * FROM user WHERE name LIKE :name LIMIT 1")
    suspend fun findByName(name: String): User?

    @Query("SELECT * FROM user ORDER BY score DESC")
    suspend fun getRanking(): List<User>

    @Insert
    suspend fun create(user: User): Long
}