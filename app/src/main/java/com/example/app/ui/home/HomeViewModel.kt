package com.example.app.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.app.data.dao.UserDao
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    var userDao: UserDao
): ViewModel() {

    private val _text = MutableLiveData<String>()
    val text: LiveData<String> get() = _text

    init {
        getRanking()
    }

    fun getRanking() {
        GlobalScope.launch {
            val users = userDao.getRanking()
            withContext(Dispatchers.Main) {
                _text.value = users.toString()
            }
        }
    }
}