package com.example.app.ui.admin

import androidx.lifecycle.*
import com.example.app.data.dao.Cat
import com.example.app.data.dao.CatDao
import com.example.app.utils.pmap
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import javax.inject.Inject

@HiltViewModel
class AdminViewModel @Inject constructor(
    var catDao: CatDao) : ViewModel() {

    private val _adminForm = MutableLiveData<String>()
    val adminFormState: LiveData<String> = _adminForm

    private val _catList = MutableLiveData<MutableList<Cat>>(mutableListOf())
    val catList: MutableLiveData<MutableList<Cat>> get() = _catList

    init {
        listCatRace()
    }

    fun addCatRace(catRace: String) {
        GlobalScope.launch {
            val c = Cat(null, catRace)
            catDao.insertRace(c)
            withContext(Dispatchers.Main) {
                _catList.value?.add(c)
                _adminForm.value = "Added cat $catRace"
            }
        }
    }

    private fun listCatRace() {
        GlobalScope.launch {
            val cats = catDao.getAll()
            withContext(Dispatchers.Main) {
                _catList.value = cats.toMutableList()
            }
        }
    }

    fun catDataChanged(catRace: String) {
        if (catRace.isEmpty()) {
            _adminForm.value = "Error cat empty"
            return
        }

        _adminForm.value = "Valid cat"
    }
}