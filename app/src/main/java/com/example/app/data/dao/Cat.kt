package com.example.app.data.dao

import androidx.room.*

@Entity(tableName = "chat")
data class Cat(
    @PrimaryKey(autoGenerate = true)
    val id: Long?,
    @ColumnInfo(name = "race")
    val race: String,
)

@Dao
interface CatDao {
    @Query("SELECT * FROM chat")
    suspend fun getAll(): List<Cat>

    @Query("SELECT * FROM chat WHERE race LIKE :race LIMIT 1")
    suspend fun findByRace(race: String): Cat

    @Insert
    suspend fun insertRace(vararg race: Cat)
}