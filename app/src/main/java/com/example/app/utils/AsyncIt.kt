package com.example.app.utils

import kotlinx.coroutines.*

suspend fun <A, B> Iterable<A>.pmap(f: suspend (A) -> B): Unit = coroutineScope {
    map { async { f(it) } }.awaitAll()
}
