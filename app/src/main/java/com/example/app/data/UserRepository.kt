package com.example.app.data

import android.util.Log
import com.example.app.data.dao.User
import com.example.app.data.dao.UserDao
import com.example.app.data.model.LoggedInUser
import java.io.IOException


/**
 * Class that requests authentication and user information from the remote data source and
 * maintains an in-memory cache of login status and user credentials information.
 */

class UserRepository(private val userDao: UserDao) {

    // in-memory cache of the loggedInUser object
    var user: LoggedInUser? = null
        private set

    val isLoggedIn: Boolean
        get() = user != null

    init {
        // If user credentials will be cached in local storage, it is recommended it be encrypted
        // @see https://developer.android.com/training/articles/keystore
        user = null
    }

    fun logout() {
        user = null
    }

    suspend fun login(username: String): Result<LoggedInUser> {
        var result: Result<LoggedInUser>
        // handle login
        try {
            // handle loggedInUser authentication
            val u = this.getOrCreate(username)
            // Current score need to be at 0 after login (begin game)
            val user = LoggedInUser(u.id.toString(), u.name, 0)
            result =  Result.Success(user)
        } catch (e: Throwable) {
            Log.e("Error", "Logging process gives : ", e)
            result = Result.Error(IOException("Error logging in", e))
        }

        if (result is Result.Success<LoggedInUser>) {
            setLoggedInUser(result.data)
        }

        return result
    }

    private suspend fun getOrCreate(name: String): User {
        return userDao.findByName(name) ?: userDao.findById(
            userDao.create(User(null, name, 0))
        )
    }

    private fun setLoggedInUser(loggedInUser: LoggedInUser) {
        this.user = loggedInUser
        // If user credentials will be cached in local storage, it is recommended it be encrypted
        // @see https://developer.android.com/training/articles/keystore
    }
}