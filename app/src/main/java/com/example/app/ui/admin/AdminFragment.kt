package com.example.app.ui.admin

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.example.app.databinding.FragmentAdminBinding
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.WithFragmentBindings

@WithFragmentBindings
@AndroidEntryPoint
class AdminFragment : Fragment() {

    private val adminViewModel: AdminViewModel by viewModels()
    private var _binding: FragmentAdminBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAdminBinding.inflate(inflater, container, false)
        val root: View = binding.root

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.inputAddCat.addTextChangedListener(afterTextChangedListener())

        adminViewModel.adminFormState.observe(viewLifecycleOwner,
            Observer { catResult ->
                catResult ?: return@Observer
                binding.validerChat.isEnabled = true
            })

        binding.validerChat.setOnClickListener {
            adminViewModel.addCatRace(catRace = binding.inputAddCat.text.toString())
        }

        adminViewModel.catList.observe(viewLifecycleOwner,
            Observer {
                if (it.isEmpty()) return@Observer
                binding.textCatList.text = it.toString()
        })
    }

    fun afterTextChangedListener() = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            // ignore
        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            // ignore
        }

        override fun afterTextChanged(s: Editable) {
            adminViewModel.catDataChanged(s.toString())
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}