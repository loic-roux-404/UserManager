package com.example.app

import android.content.Context
import com.example.app.data.UserRepository
import com.example.app.data.dao.AppDatabase
import com.example.app.data.dao.CatDao
import com.example.app.data.dao.UserDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RoomModule {

    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext appContext: Context): AppDatabase {
        return AppDatabase.getDatabase(appContext)
    }

    @Singleton
    @Provides
    fun providesUserDao(db: AppDatabase): UserDao {
        return db.userDao()
    }

    @Singleton
    @Provides
    fun providesCatDao(db: AppDatabase): CatDao {
        return db.catDao()
    }

    @Singleton
    @Provides
    fun userRepository(userDao: UserDao): UserRepository {
        return UserRepository(userDao)
    }
}